Meteor.methods
  createProduct: (product) ->
    throwNotAuthorizedError() unless isAdmin()

    data = _.extend(product,
      createdAt: new Date()
      owner: Meteor.userId()
      bidCount: 0
    )
    Products.insert(data)

  deleteProduct: (id) ->
    throwNotAuthorizedError() unless isAdmin()

    Products.remove id

  updateProduct: (product) ->
    throwNotAuthorizedError() unless isAdmin()

    Products.update product.id, { $set: _(product).omit('id') }