@requireLogin = ->
  unless Meteor.user()
    if Meteor.loggingIn()
      @render @loadingTemplate
    else
      @render "accessDenied"
  else
    @next()

Router.configure
  layoutTemplate: 'MainLayout'
  loadingTemplate: 'loading'
  waitOn: ->
    Meteor.subscribe('products')

Router.onBeforeAction @requireLogin

Router.onBeforeAction ->
  clearErrors()
  @next()

Router.route '/', ->
  name: 'home.index'
  @render 'index'

Router.route '/profile', ->
  name: 'profile.show'
  @layout 'ProfileLayout'
  @render 'profile'

Router.route '/dashboard', ->
  name: 'adminDashboard'
  @layout 'AdminLayout'
  @render 'dashboard'