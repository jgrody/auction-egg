@isAdmin = ->
  Meteor.userId() and Roles.userIsInRole(Meteor.user(), ['admin'])

@createNotAuthorizedError = ->
  return new Meteor.Error(
    'not-authorized',
    'You must be a logged-in admin to perform this action.'
  )

@throwNotAuthorizedError = ->
  error = createNotAuthorizedError();
  throw error