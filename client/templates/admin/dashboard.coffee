EDITING_KEY = 'editingProduct'
DEFAULT_EDITING_VALUE = null

NEW_PRODUCT_FORM = '#new-product-form'
NEW_PRODUCT_IMAGE = '.product-image'

resetSessionEditing = ->
  Session.set EDITING_KEY, DEFAULT_EDITING_VALUE

Template.dashboard.helpers
  products: ->
    Products.find({}, sort: createdAt: -1)

  productCount: ->
    Products.find({}).count()

  imageUrl: ->
    _(imageUrl).bind(@)()

  editingProduct: ->
    Session.equals EDITING_KEY, @_id


# Functions
getProductFormData = ->
  $(NEW_PRODUCT_FORM).objectifyForm()


resetForm = ->
  $(NEW_PRODUCT_FORM).get(0).reset()
  $(NEW_PRODUCT_IMAGE).val('')


getImagePath = (imageResponse) ->
  imageResponse?.relative_url or ''


uploadImage = (imageFiles) ->
  error = createNotAuthorizedError()
  unless isAdmin
    throwError error.reason
    throw error

  S3upload imageFiles, '/products', (error, response) ->
    # Show any S3 upload errors
    if error
      throwError error.reason

    # Otherwise create the product and set its `image` property
    # with the returned uploaded image `secure_url`
    else
      createProduct(
        _.extend(getProductFormData(),
          imagePath: getImagePath(response)
        )
      )


createProduct = (data) ->
  Meteor.call 'createProduct', data, (error, id) ->
    if error
      throwError error.reason
    else
      resetForm()


uploadProductAndImage = (imageFiles) ->
  # If file input contains a file upload to S3
  if imageFiles.length > 0
    uploadImage imageFiles

  # Otherwise just upload product without image data
  else
    createProduct getProductFormData()


# Dashboard events
Template.dashboard.events
  'submit #new-product-form': (e) ->
    e.preventDefault()
    fileData = $('input.product-image')[0].files

    uploadProductAndImage fileData

  'click .delete-product': (e) ->
    product = Products.findOne(@_id)

    Meteor.call 'deleteProduct', @_id, (error, id) ->
      if error
        throwError error.reason
      else
        S3delete '/' + product.imagePath, (error, response) ->
          throwError error.reason if error


  'click .edit-product': (e) ->
    Session.set 'editingProduct', @_id

  'click .update-product': (e) ->
    e.preventDefault()
    e.stopPropagation()

    row = $(e.target).parents('tr');

    data =
      id:    @_id
      name:  row.find('.name-input').val()
      specs: row.find('.specs-input').val()
      price: row.find('.price-input').val()

    Meteor.call 'updateProduct', data, (error, id) ->
      throwError error.reason if error

    resetSessionEditing()

  'click .cancel-update': (e) ->
    resetSessionEditing()

  'click button.upload': ->
