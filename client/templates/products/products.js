Template.index.helpers({
  products: function(){
    return Products.find({}, { sort: { createdAt: -1 } });
  }
});

Template.product.helpers({
  imageUrl: function(){
    return _(imageUrl).bind(this)();
  }
})