@bucketHost = 'https://egg-basket.s3.amazonaws.com/'

@imageUrl = ->
  if @seeded then @imagePath else bucketHost + @imagePath

@S3upload = (files, path, callback) ->
  S3.upload files, path, callback

@S3delete = (path, callback) ->
  S3.delete path, callback