buildProduct = (product) ->
  _.extend {}, product,
    createdAt: new Date()
    bidCount: 0
    price: 0
    seeded: true
    imagePath: "/images/macbook.png"

@insertProducts = ->
  products = [
    {
      name: "Product 1"
      specs: "Lorem ipsum dolor sit amet, nec habeo fastidii ne, duo et dolor deserunt iracundia, ex his falli phaedrum referrentur."
    }
    {
      name: "Product 2"
      specs: "Lorem ipsum dolor sit amet, nec habeo fastidii ne, duo et dolor deserunt iracundia, ex his falli phaedrum referrentur."
    }
    {
      name: "Product  3"
      specs: "Lorem ipsum dolor sit amet, nec habeo fastidii ne, duo et dolor deserunt iracundia, ex his falli phaedrum referrentur."
    }
  ]
  _.each products, (product) ->
    Products.insert buildProduct(product)